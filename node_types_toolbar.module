<?php

/**
 * @file
 * Contains node_types_toolbar.module.
 */

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\node_types_toolbar\ToolbarHandler;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 *
 * @inheritdoc
 */
function node_types_toolbar_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.node_types_toolbar':
      $text = file_get_contents(dirname(__FILE__) . '/README.md');
      if (!\Drupal::moduleHandler()->moduleExists('markdown')) {
        return '<pre>' . $text . '</pre>';
      }
      else {
        // Use the Markdown filter to render the README.
        $filter_manager = \Drupal::service('plugin.manager.filter');
        $settings = \Drupal::configFactory()->get('markdown.settings')->getRawData();
        $config = ['settings' => $settings];
        $filter = $filter_manager->createInstance('markdown', $config);
        return $filter->process($text, 'en');
      }
  }
  return NULL;
}

/**
 * Implements hook_menu_links_discovered_alter().
 */
function node_types_toolbar_menu_links_discovered_alter(&$links) {

  // Break node types into groups by starting letter.
  foreach ($links as $key => $link) {

    if (!empty($link['parent']) && $link['parent'] == 'entity.node_type.collection') {

      if (empty($link['title'])) {
        continue;
      }

      $link_title = $link['title'];

      if ($link['title'] instanceof TranslatableMarkup) {
        $link_title = $link['title']->render();
      }

      // Remove punctuation before alphabetizing.
      $link_title = str_replace(['"', "'"], "", $link_title);

      $group_name = substr($link_title, 0, 1);

      $parent = 'entity.node_type.collection';

      $links['entity.node_type.collection.' . $group_name] = [
        'title' => $group_name,
        'route_name' => 'entity.node_type.collection',
        'menu_name' => 'admin',
        'parent' => $parent,
      ];
      $links[$key]['parent'] = 'entity.node_type.collection.' . $group_name;
    }
  }

  $links['node.type_add'] = [
    'title' => new TranslatableMarkup('Add Node Type'),
    'route_name' => 'node.type_add',
    'menu_name' => 'admin',
    'parent' => 'entity.node_type.collection',
    'weight' => -100,
  ];

}

/**
 * Implements hook_toolbar_alter().
 */
function node_types_toolbar_toolbar_alter(&$items) {

  if (!empty($items['node_types'])) {
    $items['node_types']['#attached']['library'][] = 'admin_toolbar/toolbar.tree';
  }

  // Break up into groups of no more than seven.
  if (!empty($items['node_types']['tray']['node_types_menu'][0]['#items'])) {
    foreach ($items['node_types']['tray']['node_types_menu'][0]['#items'] as $key => &$link) {
      if (count($link['below']) > 7) {
        $copy = $link;
        $copy['below'] = array_slice($copy['below'], 7);
        $first_item_key = array_keys($copy['below'])[0];
        $items['node_types']['tray']['node_types_menu'][0]['#items'][$key . $first_item_key] = $copy;
        $link['below'] = array_slice($link['below'], 0, 7);
      }

      if (empty($link['title'])) {
        continue;
      }
    }

    ksort($items['node_types']['tray']['node_types_menu'][0]['#items']);
  }
}

/**
 * Implements hook_toolbar().
 */
function node_types_toolbar_toolbar() {
  return \Drupal::service('class_resolver')
    ->getInstanceFromDefinition(ToolbarHandler::class)
    ->toolbar();
}
